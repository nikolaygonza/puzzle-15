﻿namespace Interfaces
{
    public interface ISerializer
    {
        void SaveState();
        bool LoadState();
        void ClearData();
    }
}
