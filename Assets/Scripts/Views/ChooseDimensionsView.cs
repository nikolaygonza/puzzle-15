﻿using Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    public class ChooseDimensionsView : UIView
    {
        #region Events

        /// <summary>
        /// Event called when user choose board size
        /// </summary>
        public delegate void SizeChosen(GameController.TableDimensions size);
        public event SizeChosen OnSizeChosen;

        #endregion

        #region Variables

        /// <summary>
        /// Serialized properties to set listeners via code
        /// </summary>
        [SerializeField] private Button field3Button;
        [SerializeField] private Button field4Button;
        [SerializeField] private Button field5Button;

        #endregion

        #region Unity methods

        private void Start()
        {
            field3Button.onClick.AddListener(ChooseFirstField);
            field4Button.onClick.AddListener(ChooseSecondField);
            field5Button.onClick.AddListener(ChooseThirdField);
        }

        #endregion

        #region Public methods

        public override void ShowView()
        {
            base.ShowView();
        }

        public override void HideView()
        {
            base.HideView();
        }

        #endregion

    
        #region Private methods

        /// <summary>
        /// Button functions to implement on click events
        /// </summary>
        private void ChooseFirstField()
        {
            OnSizeChosen?.Invoke(GameController.TableDimensions.Field3X3);
        }

        private void ChooseSecondField()
        {
            OnSizeChosen?.Invoke(GameController.TableDimensions.Field4X4);
        }

        private void ChooseThirdField()
        {
            OnSizeChosen?.Invoke(GameController.TableDimensions.Field5X5);
        }

        #endregion
    }
}
