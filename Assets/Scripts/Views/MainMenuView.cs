﻿using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    public class MainMenuView : UIView
    {
        #region Events

        /// <summary>
        /// Do not need serializable callbacks, using c# events due to performance
        /// <see href="https://www.jacksondunstan.com/articles/3335"> According this article </see>
        /// </summary>

        public delegate void NewGameButtonPressed();
        public event NewGameButtonPressed OnNewGameButtonPressed;

        public delegate void QuitAppPressed();
        public event QuitAppPressed OnQuitAppPressed;

        #endregion

        #region Variables

        [SerializeField] private Button newGameButton;
        [SerializeField] private Button quitAppButton;

        #endregion

        #region Unity methods

        // Start is called before the first frame update
        void Start()
        {
            newGameButton.onClick.AddListener(StartNewGame);
            quitAppButton.onClick.AddListener(QuitApp);
        }
    
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                QuitApp();
            }
        }

        #endregion

        #region Private methods

        private void StartNewGame()
        {
            OnNewGameButtonPressed?.Invoke();
        }

        private void QuitApp()
        {
            OnQuitAppPressed?.Invoke();
        }

        #endregion

    }
}
