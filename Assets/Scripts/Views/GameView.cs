﻿using Controllers;
using UnityEngine;
using Views;

namespace Views
{
    public class GameView : UIView
    {
        #region Events

        public delegate void CancelButtonPressed();

        public event CancelButtonPressed OnCancelButtonPressed;

        public delegate void ContinueButtonPressed();

        public event ContinueButtonPressed OnContinueButtonPressed;

        public delegate void NewGameButtonPressed();

        public event NewGameButtonPressed OnNewGameButtonPressed;

        public delegate void GameScreenChosen(GameController.TableDimensions screen);

        public event GameScreenChosen OnGameScreenChosen;

        #endregion

        #region Variables

        [Header("Views")] public ContinueGameView continueGameView;
        public ChooseDimensionsView chooseDimensionsView;
        public PuzzleView puzzleView;
        public GameResultView gameResultView;

        #endregion

        #region Public methods

        public override void ShowView()
        {
            base.ShowView();
            CloseViews();
            SubscribeToEvents();
        }

        public override void HideView()
        {
            base.HideView();
            CloseViews();
            UnSubscribeToEvents();
        }

        #endregion

        #region Private methods

        private void SubscribeToEvents()
        {
            continueGameView.OnNoButtonPressed += StartNewGame;
            continueGameView.OnYesButtonPressed += ContinueGame;
            continueGameView.OnCloseButtonPressed += BackToMainMenu;
            chooseDimensionsView.OnSizeChosen += ChooseScreenSize;
            puzzleView.OnGoToMenuButtonPressed += BackToMainMenu;
            puzzleView.OnStartNewGameButtonPressed += StartNewGame;
        }

        private void UnSubscribeToEvents()
        {
            continueGameView.OnNoButtonPressed -= BackToMainMenu;
            continueGameView.OnYesButtonPressed -= ContinueGame;
            continueGameView.OnCloseButtonPressed -= BackToMainMenu;
            chooseDimensionsView.OnSizeChosen -= ChooseScreenSize;
            puzzleView.OnGoToMenuButtonPressed -= BackToMainMenu;
            puzzleView.OnStartNewGameButtonPressed -= StartNewGame;
        }

        private void StartNewGame()
        {
            OnNewGameButtonPressed?.Invoke();
        }

        private void BackToMainMenu()
        {
            OnCancelButtonPressed?.Invoke();
        }

        private void ContinueGame()
        {
            OnContinueButtonPressed?.Invoke();
        }

        private void ChooseScreenSize(GameController.TableDimensions screen)
        {
            OnGameScreenChosen?.Invoke(screen);
        }

        private void CloseViews()
        {
            continueGameView.HideView();
            chooseDimensionsView.HideView();
            puzzleView.HideView();
        }

        #endregion

    }
}
