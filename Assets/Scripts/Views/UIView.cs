﻿using Interfaces;
using UnityEngine;

namespace Views
{
    public abstract class UIView : MonoBehaviour, IView
    {
        #region Public methods

        /// <summary>
        /// Base method for showing view
        /// </summary>
        public virtual void ShowView()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Base method for hiding view 
        /// </summary>
        public virtual void HideView()
        {
            gameObject.SetActive(false);
        }

        #endregion
    }
}
