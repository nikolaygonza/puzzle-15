﻿using System;
using System.Collections.Generic;
using Interfaces;

namespace Models
{
    [Serializable]
    public class GameStateModel : IObservable
    {
        /// <summary>
        /// Main model of puzzle state
        /// </summary>
        public GameStateModelData data;
    
        /// <summary>
        /// Detects if puzzle is solve for save state
        /// </summary>
        public bool solved;
    
        /// <summary>
        /// Current observers list to notify
        /// </summary>
        private List<IObserver> observers = new List<IObserver>();

        /// <summary>
        /// Swap data in array
        /// </summary>
        /// <param name="firstX"></param>
        /// <param name="firstY"></param>
        /// <param name="secondX"></param>
        /// <param name="secondY"></param>
        public void SwapData(int firstX, int firstY, int secondX, int secondY)
        {
            var firstData = data.pieces[firstX, firstY];
            data.pieces[firstX, firstY] = data.pieces[secondX, secondY];
            data.pieces[secondX, secondY] = firstData;
        }

        /// <summary>
        /// Get's current empty piece position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void GetCurrentEmptyPosition(out int x, out int y)
        {
            x = 0; 
            y = 0;
            for (int i = 0; i < data.pieces.GetLength(0); i++)
            {
                for (int j = 0; j < data.pieces.GetLength(1); j++)
                {
                    var piece = data.pieces[j, i];
                    if (piece.index == -1)
                    {
                        x = j;
                        y = i;
                    }
                }
            }
        }

        public void AddObserver(IObserver o)
        {
            observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            observers.Remove(o);
        }

        public void NotifyObservers()
        {
            foreach (var observer in observers)
            {
                observer.Update(this);
            }
        }
    }
}
