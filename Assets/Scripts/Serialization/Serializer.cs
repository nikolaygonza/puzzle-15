﻿using Interfaces;

namespace Serialization
{
    /// <summary>
    /// Serializer abstract class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Serializer<T> : ISerializer
    {
        protected T data;

        public virtual void SetData(T data)
        {
            this.data = data;
        }

        public virtual T GetData()
        {
            return data;
        }

        public virtual void SaveState()
        {
            return;
        }

        public virtual bool LoadState()
        {
            return false;
        }

        public virtual void ClearData()
        {
            return;
        }
    }
}
