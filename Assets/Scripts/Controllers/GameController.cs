﻿using System.Collections;
using Models;
using Serialization;
using UnityEngine;
using Views;
using Random = UnityEngine.Random;

namespace Controllers
{
    public class GameController : SubController<GameView>
    {
        #region Enums

        /// <summary>
        /// Enum of table size for better communication
        /// </summary>
        public enum TableDimensions
        {
            Field3X3 = 9,
            Field4X4 = 16,
            Field5X5 = 25
        }

        #endregion

        #region Variables

        /// <summary>
        /// Serializer for game state, implemented PlayerPrefs
        /// </summary>
        private Serializer<GameStateModelData> gameSerializer;

        /// <summary>
        /// Current game model
        /// </summary>
        private GameStateModel currentModel;

        [SerializeField] [Range(0.01f, 2f)] private float animationTime;
        [SerializeField] [Range(1, 100)] private int shuffleMovesCount;

        private bool isTimerActive;

        #endregion

        #region Unity methods

        //Save game state if leave
        private void OnApplicationQuit()
        {
            SaveGameState();
        }

        //Save game state on unfocus
        private void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus)
                SaveGameState();
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Subscribes to all ui events
        /// </summary>
        private void SubscribeToEvents()
        {
            ui.OnCancelButtonPressed += CloseGameView;
            ui.OnContinueButtonPressed += ContinueGame;
            ui.OnNewGameButtonPressed += ChooseTableSize;
            ui.OnGameScreenChosen += StartNewGame;
            ui.puzzleView.OnBoardPieceClick += OnPieceClick;
            ui.puzzleView.OnPuzzleSolved += OnPuzzleSolved;
            ui.gameResultView.GameResultClosed += CloseGameView;
        }

        /// <summary>
        /// Unsubscribes from all ui events
        /// </summary>
        private void UnsubscribeFromEvents()
        {
            ui.OnCancelButtonPressed -= CloseGameView;
            ui.OnContinueButtonPressed -= ContinueGame;
            ui.OnNewGameButtonPressed -= ChooseTableSize;
            ui.OnGameScreenChosen -= StartNewGame;
            ui.puzzleView.OnBoardPieceClick -= OnPieceClick;
            ui.puzzleView.OnPuzzleSolved -= OnPuzzleSolved;
            ui.gameResultView.GameResultClosed -= CloseGameView;
            StopTimer();
        }

        /// <summary>
        /// Continue game if possible
        /// </summary>
        private void ContinueGame()
        {
            OpenPuzzleView();
            currentModel.data = gameSerializer.GetData();
            currentModel.NotifyObservers();
            StartGameTimer();
        }

        /// <summary>
        /// Starting new game
        /// </summary>
        /// <param name="dimensions"> Chosen board size </param>
        private void StartNewGame(TableDimensions dimensions)
        {
            //clear saved data
            gameSerializer.ClearData();
            currentModel.data.currentScreenDimensions = (int) dimensions;
            OpenPuzzleView();
            CreatePuzzleBoard();
        }

        private void OpenPuzzleView()
        {
            ui.puzzleView.ShowView();
            ui.puzzleView.SetAnimationTime(animationTime);
            currentModel.AddObserver(ui.puzzleView.observer);
        }

        /// <summary>
        /// Open view to choose board size
        /// </summary>
        private void ChooseTableSize()
        {
            if (!currentModel.data.IsNull())
            {
                StopTimer();
                ui.puzzleView.HideView();
                currentModel = new GameStateModel();
            }

            ui.chooseDimensionsView.ShowView();
        }

        /// <summary>
        /// Fill board with chosen size pieces with default position
        /// </summary>
        private void CreatePuzzleBoard()
        {
            GetTableDimensions(out var x, out var y);
            BoardPieceData[,] pieces = new BoardPieceData[x, y];
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    if (i == y - 1 && j == x - 1)
                    {
                        pieces[j, i] = new BoardPieceDataBuilder().SetIndex(-1).Build();
                        continue;
                    }

                    pieces[j, i] = new BoardPieceDataBuilder().SetCorrectPosition(j, i).SetIndex(i * x + j).Build();
                }
            }

            currentModel.data.pieces = pieces;
            currentModel.NotifyObservers();
            StartCoroutine(ShuffleBoard());
        }

        /// <summary>
        /// Method for shuffling board <see cref="shuffleMovesCount" /> times with random count but vert/hor moves
        /// </summary>
        /// <returns></returns>
        private IEnumerator ShuffleBoard()
        {
            ui.puzzleView.shuffling = true;
            GetTableDimensions(out var x, out var y);
            for (int i = 0; i < shuffleMovesCount; i++)
            {
                currentModel.GetCurrentEmptyPosition(out var currentEmptyPositionX, out var currentEmptyPositionY);
                bool verticalMove = i % 2 == 0;
                if (verticalMove)
                {
                    //generate random position
                    int yPiece = Random.Range(0, y);

                    //check if random position is in empty piece
                    if (yPiece == currentEmptyPositionY)
                    {
                        if (yPiece > 1)
                            yPiece--;
                        else
                        {
                            yPiece++;
                        }
                    }

                    ResolveVerticalMove(currentEmptyPositionY, yPiece, currentEmptyPositionX);
                }
                else
                {
                    int xPiece = Random.Range(0, x);
                    if (xPiece == currentEmptyPositionX)
                    {
                        if (xPiece > 1)
                            xPiece--;
                        else
                        {
                            xPiece++;
                        }
                    }

                    ResolveHorizontalMove(currentEmptyPositionX, xPiece, currentEmptyPositionY);
                }

                //No need if model is solvable because it's generated from origin position
                currentModel.NotifyObservers();
                yield return new WaitForSeconds(animationTime);
            }

            ui.puzzleView.shuffling = false;
            StartGameTimer();
        }

        /// <summary>
        /// Can be done just by sqrt but may be not square size are possible
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void GetTableDimensions(out int x, out int y)
        {
            switch ((TableDimensions) currentModel.data.currentScreenDimensions)
            {
                case TableDimensions.Field3X3:
                    x = 3;
                    y = 3;
                    break;
                case TableDimensions.Field4X4:
                    x = 4;
                    y = 4;
                    break;
                case TableDimensions.Field5X5:
                    x = 5;
                    y = 5;
                    break;
                default:
                    x = 0;
                    y = 0;
                    break;
            }
        }

        private void StartGameTimer()
        {
            isTimerActive = true;
            StartCoroutine(TimerRoutine());
        }

        private void StopTimer()
        {
            isTimerActive = false;
        }

        private IEnumerator TimerRoutine()
        {
            yield return new WaitForSeconds(1);
            if (isTimerActive)
            {
                currentModel.data.time++;
                currentModel.NotifyObservers();
                StartCoroutine(TimerRoutine());
            }
        }

        private void OnPieceClick(BoardPiece piece)
        {
            //get current piece position
            int x = piece.x;
            int y = piece.y;

            //get empty position
            currentModel.GetCurrentEmptyPosition(out var currentEmptyPositionX, out var currentEmptyPositionY);

            // detects if click is in the same row/column 
            if (currentEmptyPositionX != x && currentEmptyPositionY != y)
                return;

            // get move direction
            if (currentEmptyPositionX == x)
            {
                ResolveVerticalMove(currentEmptyPositionY, y, x);
            }
            else
            {
                ResolveHorizontalMove(currentEmptyPositionX, x, y);
            }

            //add move
            currentModel.data.moves++;

            //update views
            currentModel.NotifyObservers();
        }

        private void ResolveVerticalMove(int currentEmptyPositionY, int y, int x)
        {
            var direction = currentEmptyPositionY > y ? -1 : 1;
            while (currentEmptyPositionY != y)
            {
                currentModel.SwapData(x, currentEmptyPositionY, x, currentEmptyPositionY + direction);
                currentEmptyPositionY += direction;
            }
        }

        private void ResolveHorizontalMove(int currentEmptyPositionX, int x, int y)
        {
            var direction = currentEmptyPositionX > x ? -1 : 1;
            while (currentEmptyPositionX != x)
            {
                currentModel.SwapData(currentEmptyPositionX, y, currentEmptyPositionX + direction, y);
                currentEmptyPositionX += direction;
            }
        }

        private void OnPuzzleSolved()
        {
            currentModel.solved = true;
            StopTimer();
            ui.gameResultView.ShowView();
            ui.gameResultView.SetVariables(currentModel.data.moves, currentModel.data.time);
        }

        private void SaveGameState()
        {
            if (!currentModel.data.IsNull())
            {
                gameSerializer.SetData(currentModel.data);
                gameSerializer.SaveState();
            }
        }

        /// <summary>
        /// Closes the whole game view
        /// </summary>
        private void CloseGameView()
        {
            mainController.SetState(MainController.ControllerType.MainMenuController);
        }

        #endregion

        #region Public methods


        public override void StartController()
        {
            base.StartController();

            SubscribeToEvents();

            //creating new model
            if (currentModel == null)
                currentModel = new GameStateModel();

            //creating serializer
            if (gameSerializer == null)
                gameSerializer = new PlayerPrefsSerializer();

            if (gameSerializer.LoadState())
            {
                ui.continueGameView.ShowView();
            }
            else
            {
                ChooseTableSize();
            }
        }

        public override void StopController()
        {
            base.StopController();
            UnsubscribeFromEvents();
            if (currentModel != null && !currentModel.solved)
            {
                gameSerializer.SetData(currentModel.data);
                gameSerializer.SaveState();
            }
            else
            {
                gameSerializer?.ClearData();
            }

            gameSerializer = null;
            currentModel = null;
        }

        #endregion
    }
}