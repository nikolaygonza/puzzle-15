﻿using UnityEngine;

namespace Controllers
{
    public class MainController : MonoBehaviour
    {
        #region Enums

        public enum ControllerType
        {
            MainMenuController,
            GameController
        }

        #endregion

        #region Variables

        /// <summary>
        /// Reference to current state
        /// </summary>
        private SubController _currentState;

        /// <summary>
        /// List of all sub controllers
        /// </summary>
        [Header("Controllers")] 
        [SerializeField] private MainMenuController mainMenuController;
        [SerializeField] private GameController gameController;

        #endregion

        #region Unity methods

        // Start is called before the first frame update
        void Start()
        {
            StopAllControllers();
            SetChildControllers();
            SetState(ControllerType.MainMenuController);
        }

        #endregion

        #region Private methods

        private void SetChildControllers()
        {
            mainMenuController.mainController = this;
            gameController.mainController = this;
        }

        private void StopAllControllers()
        {
            mainMenuController.StopController();
            gameController.StopController();
        }

        #endregion

        #region Public methods

        public void SetState(ControllerType type)
        {
            if (_currentState != null)
                _currentState.StopController();
            switch (type)
            {
                case ControllerType.MainMenuController:
                    _currentState = mainMenuController;
                    break;
                case ControllerType.GameController:
                    _currentState = gameController;
                    break;
                //case ControllerType.GameOverController:

            }

            _currentState.StartController();
        }

        #endregion

    }
}