﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Views
{


    public class BoardPiece : MonoBehaviour, IPointerClickHandler
    {
        #region Events

        public delegate void StartMove();

        public event StartMove OnStartMove;

        public delegate void FinishMove();

        public event FinishMove OnFinishMove;

        public delegate void PieceClick(BoardPiece piece);

        public PieceClick OnPieceClick;

        #endregion

        #region Variables

        [SerializeField] private TMP_Text pieceNumber;
        private RectTransform rectTransform;

        public int x, y;

        #endregion

        #region Properties

        public RectTransform RectTransform
        {
            get
            {
                if (rectTransform == null)
                    rectTransform = GetComponent<RectTransform>();
                return rectTransform;
            }
        }

        public bool IsCorrect(int correctX, int correctY)
        {
            return x == correctX && y == correctY;
        }

        #endregion

        #region Public methods

        public void MovePiece(int x, int y, float time)
        {
            OnStartMove?.Invoke();
            this.x = x;
            this.y = y;
            StartCoroutine(MovePieceRoutine(new Vector2(x * rectTransform.rect.width, -y * rectTransform.rect.height),
                time));
        }



        public void SetNumber(int number)
        {
            pieceNumber.text = number.ToString();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnPieceClick?.Invoke(this);
        }

        #endregion

        #region Private methods

        private IEnumerator MovePieceRoutine(Vector2 position, float time)
        {
            Vector2 startPos = RectTransform.anchoredPosition;
            float currentTime = 0f;
            while (currentTime < time)
            {
                RectTransform.anchoredPosition = Vector2.Lerp(startPos, position, currentTime / time);
                currentTime += Time.deltaTime;
                yield return null;
            }

            RectTransform.anchoredPosition = position;
            OnFinishMove?.Invoke();
        }

        #endregion

    }
}
