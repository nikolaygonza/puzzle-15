﻿using Interfaces;
using Models;

namespace Observers
{
    /// <summary>
    /// Observer class for updating game state
    /// </summary>
    public class GameStateObserver : IObserver
    {
        #region Events

        public delegate void GameStateChanged(GameStateModelData model);
        public event GameStateChanged OnGameStateChanged;

        #endregion

        #region Public methods

        public void Update(object ob)
        {
            GameStateModel model = (GameStateModel) ob;
            OnGameStateChanged?.Invoke(model.data);
        }

        #endregion
    }
}
