﻿namespace Interfaces
{
    public interface IView
    {
        void ShowView();
        void HideView();
    }
}
