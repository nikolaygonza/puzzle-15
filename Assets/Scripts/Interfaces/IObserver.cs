﻿namespace Interfaces
{
    public interface IObserver
    {
        void Update(object ob);
    }
}
