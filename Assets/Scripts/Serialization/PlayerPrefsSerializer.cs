﻿using System;
using Models;
using Newtonsoft.Json;
using UnityEngine;

namespace Serialization
{
    /// <summary>
    /// Class is using for saving game state to player prefs
    /// </summary>
    public class PlayerPrefsSerializer : Serializer<GameStateModelData>
    {
        private readonly string PlayerPrefsKey = "GameStateKey";
    
        public override void SaveState()
        {
            if (!data.IsNull())
            {
                var stringData = JsonConvert.SerializeObject(data);
                PlayerPrefs.SetString(PlayerPrefsKey, stringData);
                PlayerPrefs.Save();
            }
        }

        public override bool LoadState()
        {
            try
            {
                if (PlayerPrefs.HasKey(PlayerPrefsKey))
                {
                    data = JsonConvert.DeserializeObject<GameStateModelData>(PlayerPrefs.GetString(PlayerPrefsKey));
                    if (!data.IsNull())
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {
                Debug.LogError($"Error while loading state from player prefs message = \n {e.Message}");
                return false;
            }
        }

        public override void ClearData()
        {
            if (PlayerPrefs.HasKey(PlayerPrefsKey))
            {
                PlayerPrefs.DeleteKey(PlayerPrefsKey);
                data = default;
            }
        }

    }
}
