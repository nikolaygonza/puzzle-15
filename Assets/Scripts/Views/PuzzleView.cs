﻿using System.Collections.Generic;
using Controllers;
using Factories;
using Models;
using Observers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    public class PuzzleView : UIView
    {
        #region Events

        public delegate void StartNewGameButtonPressed();

        public event StartNewGameButtonPressed OnStartNewGameButtonPressed;

        public delegate void GoToMenuButtonPressed();

        public event GoToMenuButtonPressed OnGoToMenuButtonPressed;

        public delegate void UIBoardPieceClick(BoardPiece piece);

        public event UIBoardPieceClick OnBoardPieceClick;

        public delegate void PuzzleSolved();

        public event PuzzleSolved OnPuzzleSolved;

        #endregion

        #region Variables

        private float animationTime;
        [SerializeField] private TMP_Text movesText;
        [SerializeField] private TMP_Text timeText;
        [SerializeField] private Button startNewGameButton;
        [SerializeField] private Button goToMenuButton;
        [SerializeField] private RectTransform puzzleField;

        /// <summary>
        /// flag not to check solved and not detect touches while shuffling the board
        /// </summary>
        [HideInInspector] public bool shuffling;

        /// <summary>
        /// Factory that creates board pieces
        /// </summary>
        [SerializeField] private BoardPieceFactory pieceFactory;

        /// <summary>
        /// Dictionary with all board pieces views
        /// </summary>
        private Dictionary<int, BoardPiece> pieces = new Dictionary<int, BoardPiece>();

        /// <summary>
        /// Observer for tracking game state model changes
        /// </summary>
        public GameStateObserver observer;

        /// <summary>
        /// Detects if user is able to move pieces
        /// </summary>
        private bool canMove = true;

        #endregion

        #region Unity methods

        // Start is called before the first frame update
        void Start()
        {
            startNewGameButton.onClick.AddListener(StartNewGame);
            goToMenuButton.onClick.AddListener(GoToMainMenu);
            AdjustFieldToSquare();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                GoToMainMenu();
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Triggers on game model changed, setting view on received model
        /// </summary>
        /// <param name="model"></param>
        private void OnGameStateChanged(GameStateModelData model)
        {
            // setting factory size
            pieceFactory.SetPieceState((GameController.TableDimensions) model.currentScreenDimensions);

            // filling texts
            movesText.text = model.moves.ToString();
            timeText.text = $"{model.time:00:00}";

            // create or move pieces
            bool created = true;
            for (int i = 0; i < model.pieces.GetLength(0); i++)
            {
                for (int j = 0; j < model.pieces.GetLength(1); j++)
                {
                    var piece = model.pieces[j, i];
                    if (piece.index == -1)
                        continue;
                    if (!pieces.ContainsKey(piece.index))
                    {
                        created = false;
                        CreateBoardPiece(j, i, piece.index);
                    }
                    else
                    {
                        var pieceGameObject = pieces[piece.index];
                        if (pieceGameObject.x != j || pieceGameObject.y != i)
                        {
                            pieceGameObject.MovePiece(j, i, shuffling ? animationTime / 3 : animationTime);
                        }
                    }
                }
            }

            if (!shuffling && created)
            {
                if (CheckIfPuzzleSolved(model))
                {
                    OnPuzzleSolved?.Invoke();
                }
            }
        }

        /// <summary>
        /// Creates board piece instance
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="index"></param>
        private void CreateBoardPiece(int x, int y, int index)
        {
            var piece = pieceFactory.BuildBoardPiece(index + 1, x, y);
            piece.OnPieceClick += OnPieceClick;
            piece.OnStartMove += PieceOnOnStartMove;
            piece.OnFinishMove += PieceOnOnFinishMove;
            pieces.Add(index, piece);
        }

        private void OnPieceClick(BoardPiece piece)
        {
            if (canMove && !shuffling)
                OnBoardPieceClick?.Invoke(piece);
        }

        private void StartNewGame()
        {
            OnStartNewGameButtonPressed?.Invoke();
        }

        private void GoToMainMenu()
        {
            OnGoToMenuButtonPressed?.Invoke();
        }

        private void AdjustFieldToSquare()
        {
            var sizeDelta = puzzleField.sizeDelta;
            sizeDelta.y = puzzleField.rect.width;
            puzzleField.sizeDelta = sizeDelta;
        }

        private void PieceOnOnFinishMove()
        {
            canMove = true;
        }

        private void PieceOnOnStartMove()
        {
            canMove = false;
        }

        private bool CheckIfPuzzleSolved(GameStateModelData model)
        {
            foreach (var piece in model.pieces)
            {
                if (!pieces.ContainsKey(piece.index)) continue;
                if (pieces[piece.index].IsCorrect(piece.correctX, piece.correctY))
                    continue;
                return false;
            }

            return true;
        }

        #endregion

        #region Public methods

        public void SetAnimationTime(float time)
        {
            animationTime = time;
        }

        public override void ShowView()
        {
            base.ShowView();
            observer = new GameStateObserver();
            observer.OnGameStateChanged += OnGameStateChanged;
        }

        public override void HideView()
        {
            base.HideView();
            observer = null;
            movesText.text = string.Empty;
            timeText.text = string.Empty;
            foreach (var piece in pieces.Values)
            {
                Destroy(piece.gameObject);
            }

            pieces.Clear();
        }

        #endregion

    }
}
