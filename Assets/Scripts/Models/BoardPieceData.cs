﻿using System;

namespace Models
{
    [Serializable]
    public struct BoardPieceData
    {
        public int correctX;
        public int correctY;
        public int index;

        public override string ToString()
        {
            return
                $"BoardPieceData correct position = {correctX} : {correctY} index = {index}";
        }
    }

    /// <summary>
    /// Fluent builder for board piece data
    /// </summary>
    public class BoardPieceDataBuilder
    {
        private BoardPieceData data;

        public BoardPieceDataBuilder SetCorrectPosition(int x, int y)
        {
            data.correctX = x;
            data.correctY = y;
            return this;
        }

        public BoardPieceDataBuilder SetIndex(int index)
        {
            data.index = index;
            return this;
        }
        public BoardPieceData Build()
        {
            return data;
        }

    }
}