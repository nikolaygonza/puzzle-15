﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    [RequireComponent(typeof(Animator))]
    public class GameResultView : UIView
    {
        #region Events

        public Action GameResultClosed;

        #endregion

        #region Variables

        [SerializeField] private float animationTime;
        [SerializeField] private Button closeButton;
        [SerializeField] private TMP_Text movesCountText;
        [SerializeField] private TMP_Text timeCountText;
        private Animator popUpAnimator;
        private static readonly int StateBool = Animator.StringToHash("PopUpState");

        #endregion

        #region Unity methods

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                HideView();
            }
        }

        #endregion

        #region Private methods

        private IEnumerator ChangeResultState(bool state)
        {
            popUpAnimator.SetBool(StateBool, state);
            yield return null;
            yield return new WaitForSeconds(animationTime);
            if (!state)
            {
                base.HideView();
                GameResultClosed?.Invoke();
            }
        }

        #endregion

        #region Public methods

        public override void ShowView()
        {
            base.ShowView();
            popUpAnimator = GetComponent<Animator>();
            closeButton.onClick.AddListener(HideView);
            StartCoroutine(ChangeResultState(true));
        }

        public override void HideView()
        {
            StartCoroutine(ChangeResultState(false));
        }

        public void SetVariables(int moves, long time)
        {
            movesCountText.text = moves.ToString();
            timeCountText.text = $"{time:00:00}";
        }

        #endregion
        
    }
}
