﻿using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    public class ContinueGameView : UIView
    {
        #region Events

        public delegate void YesButtonPressed();
        public event YesButtonPressed OnYesButtonPressed;

        public delegate void NoButtonPressed();
        public event NoButtonPressed OnNoButtonPressed;
    
        public delegate void CloseButtonPressed();
        public event CloseButtonPressed OnCloseButtonPressed;
    
        #endregion

        #region Variables

        [SerializeField] private Button yesButton;
        [SerializeField] private Button noButton;
        [SerializeField] private Button closeButton;

        #endregion
    
        #region Unity methods
        private void Start()
        {
            yesButton.onClick.AddListener(PressYesButton);
            noButton.onClick.AddListener(PressNoButton);
            closeButton.onClick.AddListener(CloseView);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                CloseView();
            }
        }

        #endregion

        #region Private methods

        private void PressYesButton()
        {
            OnYesButtonPressed?.Invoke();
        }

        private void PressNoButton()
        {
            OnNoButtonPressed?.Invoke();
        }

        private void CloseView()
        {
            OnCloseButtonPressed?.Invoke();
        }

        #endregion


    }
}
