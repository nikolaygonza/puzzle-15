﻿using System;

namespace Models
{
    [Serializable]
    public struct GameStateModelData
    {
        public int currentScreenDimensions;
        public long time;
        public BoardPieceData[,] pieces;
        public int moves;
    }

    public static class Extensions
    {
        public static bool IsNull<T>(this T source) where T:struct
        {
            return source.Equals(default(T));
        }
    }
}