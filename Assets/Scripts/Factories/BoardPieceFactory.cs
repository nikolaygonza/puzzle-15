﻿using Controllers;
using UnityEngine;
using Views;

namespace Factories
{
    /// <summary>
    /// Factory for creating board pieces
    /// </summary>
    public class BoardPieceFactory : MonoBehaviour
    {
        #region Variables

        /// <summary>
        /// Transform for creating all pieces
        /// </summary>
        [SerializeField] private RectTransform puzzleBoard;
        /// <summary>
        /// Prefab for board piece
        /// </summary>
        [SerializeField] private BoardPiece piecePrefab;

        /// <summary>
        /// Current board size
        /// </summary>
        private GameController.TableDimensions size;
    
        /// <summary>
        /// Current piece width, can be the same as height
        /// </summary>
        private float pieceWidth;
        /// <summary>
        /// Current piece height
        /// </summary>
        private float pieceHeight;
        /// <summary>
        /// Height of the panel, pieces are in
        /// </summary>
        private float totalHeight;
        /// <summary>
        /// Width of the panel, pieces are in
        /// </summary>
        private float totalWidth;

        #endregion

        #region Unity events

        private void Start()
        {
            totalHeight = puzzleBoard.rect.height;
            totalWidth = puzzleBoard.rect.width;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Setting current board size, calculates new pieces size
        /// </summary>
        /// <param name="_size"></param>
        public void SetPieceState(GameController.TableDimensions _size)
        {
            if (size == _size) return;
            size = _size;
            CalculatePieceSize();
        }

        /// <summary>
        /// Creates new instance of chess piece
        /// </summary>
        /// <param name="index"> Piece index </param>
        /// <param name="number"> Piece number displayed </param>
        /// <param name="x"> Piece row </param>
        /// <param name="y"> Piece column</param>
        /// <returns> <see cref="BoardPiece"/> instance </returns>
        public BoardPiece BuildBoardPiece(int number, int x, int y)
        {
            var piece = Instantiate(piecePrefab, puzzleBoard);
            var pieceSize = new Vector2(pieceWidth, pieceHeight);
            piece.RectTransform.anchoredPosition = pieceSize * new Vector2(x, -y);
            piece.RectTransform.sizeDelta = pieceSize;
            piece.SetNumber(number);
            piece.gameObject.SetActive(true);
            piece.MovePiece(x, y, 0);
            return piece;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Calculates piece size from board size
        /// </summary>
        private void CalculatePieceSize()
        {
            //if not square dimensions will be added
            switch (size)
            {
                case GameController.TableDimensions.Field3X3:
                    pieceWidth = totalWidth / 3;
                    pieceHeight = totalHeight / 3;
                    break;
                case GameController.TableDimensions.Field4X4:
                    pieceWidth = totalWidth / 4;
                    pieceHeight = totalHeight / 4;
                    break;
                case GameController.TableDimensions.Field5X5:
                    pieceWidth = totalWidth / 5;
                    pieceHeight = totalHeight / 5;
                    break;
            }
        }

        #endregion
    }
}
