﻿using UnityEngine;
using Views;

namespace Controllers
{
    /// <summary>
    /// Base class for all sub controllers with reference to main controller
    /// </summary>
    public abstract class SubController : MonoBehaviour
    {
        /// <summary>
        /// Main controller reference
        /// </summary>
        [HideInInspector] public MainController mainController;

        /// <summary>
        /// Methods used to start controller
        /// </summary>
        public virtual void StartController()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Methods used to stop controller
        /// </summary>
        public virtual void StopController()
        {
            gameObject.SetActive(false);
        }
    }

    public abstract class SubController<T> : SubController where T : UIView
    {
        /// <summary>
        /// View implementation of controller
        /// </summary>
        [SerializeField] protected T ui;

        public T UI => ui;

        public override void StartController()
        {
            base.StartController();
            ui.ShowView();
        }

        public override void StopController()
        {
            base.StopController();
            ui.HideView();
        }
    }
}
