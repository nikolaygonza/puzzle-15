using UnityEngine;
using Views;

namespace Controllers
{
    public class MainMenuController : SubController<MainMenuView>
    {
        #region Public methods

        public override void StartController()
        {
            base.StartController();
            SubscribeToViewEvents();
        }

        public override void StopController()
        {
            base.StopController();
            UnSubscribeToViewEvents();
        }

        #endregion

        #region Private methods

        private void SubscribeToViewEvents()
        {
            ui.OnQuitAppPressed += QuitApplication;
            ui.OnNewGameButtonPressed += StartNewGame;
        }

        private void UnSubscribeToViewEvents()
        {
            ui.OnQuitAppPressed -= QuitApplication;
            ui.OnNewGameButtonPressed -= StartNewGame;
        }

        private void QuitApplication()
        {
            if (Application.isEditor)
            {
                Debug.Log("Cant quit from editor");
            }
            else
            {
                Application.Quit();
            }
        }

        private void StartNewGame()
        {
            mainController.SetState(MainController.ControllerType.GameController);
        }

        #endregion
    }
}